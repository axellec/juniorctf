# How to contribute?

## What you can contribute to

*If you want to contribute, you are welcome!*

Tech:

- Ensure that each kid gets his/her own Docker container, and not a container shared (and possibly "polluted") by other kids...
- Modify Docker image of john 1-5 so that participants don't need to have John The Ripper on their own machine.
- Make this Windows friendly...
- Create a Docker image of Junior CTF?
- Add more challenges :)
- Create a Docker container with hydra 9.0 for `hydra2-frigo`
- Fix issue with import.py script: the description does not import nice if there are multi-line shell examples because of markdown to HTML conversion.


Not tech:

- Translate challenges to English, and other languages (**started**)
- Host an instance of Junior CTF
- Art / images for challenges

## Translations

Current status of languages for challenges:

| Challenges | Languages |
| ---------- | --------- |
| john1 | fr, en |
| john2 | fr |
| john3-boris | fr |
| john4-garage | fr |
| john5-moneypenny | fr |
| john6-voiture | fr |
| log1-mi | fr |
| net1-spectre | fr |
| prog-moyenne | fr |
| prog-supercrypt | en |
| unix1-goldeneye | fr, en |
| unix2-exfiltration | fr |
| unix3-leiter | fr |
| hydra1-frigo | fr, en |
| reverse-getin1 | en |
| reverse-getin2 | en |
| reverse-getin3 | en |
| reverse-getin4 | en |
| reverse-getin5 | en |
| reverse-getin6 | en |
| reverse-picosafe | en |
| crypto-ascii | en |
| crypto-caesar | en |
| crypto-enigma | en |
| web1-source | en |
| web2-js | en |
| web3-random | en |




## How to add a new challenge

Each challenge has its own directory with:

- `Dockerfile.xx`. To build the Docker image for this challenge. Try and keep the containers **light**, because there are several challenges! If your challenge doesn't need any container, then, you don't need it :) If your image contains messages in a given language **then use Dockerfile.xx** where xx is your language code (e.g. Dockerfile.fr)
- `Makefile`.  Optional, for example to compile executables used in the challenge.
- `FLAG.xx` where *xx* is your language code e.g. `fr`. **Mandatory**. This file contains the expected flag - that's all. You don't want to have to re-do all challenges to find the flag, I tell you. Even if they are easy. 
- `solution.md`. Optional. Explains how to solve this challenge.
- `./public` directory. **Mandatory**. Place in this directory all information you need to provide to the kids to solve the challenge. Make sure not to put anything secret in here!
- `./public/description.xx.md`. where xx is your language code. **Mandatory**. Describes the scenario for the kids to read. `fr` for French - change for another language.
- Place in `./public` any other file you need to provide. The executable to reverse engineer, an unshadowed password file, images etc.
- The challenge directory (`.`) may contain other files, for example source files used to generate an executable, tests etc. Freely organize the rest of the directory the way that suits you!


## Contributors

Thanks!!!

- ant0inet (CTFd challenge importer)
