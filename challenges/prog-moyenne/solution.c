#include <stdio.h>

void main() {
  int tableau[6];
  
  tableau[0] = 19;
  tableau[1] = 19;
  tableau[2] = 17;
  tableau[3] = 11;
  tableau[4] = 17;
  tableau[5] = 18;

  float moyenne;
  int i = 0;
  int somme = 0;

  for (i=0; i<6; i++) 
    somme = tableau[i] + somme;

  moyenne = (float) somme / 6;

  printf("Moyenne : %f\n", moyenne);
}
  
