Title: Great Ping
Category: Web
Score: 100
Description:

Our dearest competitor, Great Ping, has set up a new service to ping hosts.
What is this? Basically, it is a web service to let you try and ping any remote host you wish. Try it on [http://34.78.104.204:3336](http://34.78.104.204:3336)

Except it's damn vulnerable :=)

Find the flag!
