import java.io.*;
import java.util.Scanner;  // Import the Scanner class
import java.util.Base64;
/* To compile Java code: 
   javac SuperCrypt.java
   
   Then run it with:
   java SuperCrypt
*/

public class SuperCrypt {
    static final byte [] KEY = "GEEK".getBytes();
    static final String ENCRYPTED = "rbGmssK8qrezpKm6tapmyA==";
    
    public static String encrypt(byte [] input) {
	byte [] result = new byte[input.length];
	for (int i=0; i< input.length; i++) {
	    result[i] = (byte) (input[i] + KEY[ i % 4 ]); // % is a math operator which means modulo. i % 4 can take only 4 possible values: 0, 1, 2 and 3
	}

	/* you do not need to understand Base64. 
	   To encode, you use Base64.getEncoder()
	   To decode (the other way), you use Base64.getDecoder() */
	return new String(Base64.getEncoder().encodeToString(result));
    }

    public static String decrypt(String encrypted_value) {
	byte [] result = Base64.getDecoder().decode(encrypted_value);

	/* you need to write code here */

	
	return new String(result); /* transforms a byte[] to a String */
    }

    
    public static void main(String [] args) {
	/* you might want to modify this main() to solve the challenge ;-) */
	Scanner myObj = new Scanner(System.in); 
	System.out.print("Say the word: ");

	String input = myObj.nextLine(); 
	String encrypted_input = encrypt(input.getBytes());

	if (encrypted_input.equals(ENCRYPTED)) {
	    System.out.println("Congrats! The flag  is: "+input);
	} else {
	    System.out.println("Try harder!");
	}
	
    }

}
