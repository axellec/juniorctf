Title: Very Secret Base 2
Category: Reverse
Score: 60
Description:

This challenge is very similar to get `getin` number 1. Download `getin2`, set it as executable and run it. You should know how to do this, or go back and see Getin1.

Same, you'll need to use `strings` to find the password. But this time, the password is a bit longer, and it is split on several lines.

## Need some help?

Use the hints...

## Flag

Use the password you found to flag this challenge
