Title: John 2
Score: 100
Category: Passwords
Description:

Palpaguin a eu vent d'une breche dans son systeme de controle, et il a immediatement changé son mot de passe. Cette fois-çi, nous savons qu'il s'agit d'un fruit en anglais, et suivi d'un chiffre.

Est-ce que tu peux arriver à te logger sur la machine à nouveau ?
Tu disposes du fichier de mot de passe haché (à télécharger).

Tu auras aussi besoin d'un fichier de configuration `john.conf` (à télécharger). Je t'explique plus bas pourquoi.

## Attention

1. Cette épreuve est plus difficile que `john 1`. Je te conseille de faire d'abord john 1.
2. Bla bla, tu sais que attention, dans la vraie vie, on ne craque les mots de passe que des machines qui t'appartiennent. **Jamais celle des autres**.


## Accéder au système de controle

Tu y accèdes par SSH, sur le port 2122. Le nom d'utilisateur de palpaguin est `palpaguin` et l'adresse du serveur est `192.168.0.8`.

Tu es un pro de SSH ? Ca devrait te suffire pour arriver à accéder au système. Si tu peines, consulte à nouveau l'aide dans l'épreuve **john1**.



## Casser des mots de passe avec John

Je te conseille d'utiliser à nouveau John The Ripper. Mais cette fois-ci, il faut etre plus intelligent, ou très patient. Comme tu préfères.

Si tu as le courage d'écrire une liste de noms de fruits suivis de chiffres, très bien.

Mais tu peux faire plus subtil avec John. Tu peux lui donner une liste de mots de base, et ensuite lui dire de les modifier un peu. Cela s'appelle une règle, en anglais `rules`.

### Sauvegarde du fichier de configuration

Les règles, tu dois les écrire dans un fichier de configuration que tu places dans `/etc/john/john.conf`. Je te conseille d'abord de sauvegarder le fichier existant. Dans un terminal, tu tapes:

```
sudo cp /etc/john/john.conf /etc/john/john.conf.bak
```

### Sudo

`sudo`, c'est une commande que tu as déjà vue. Elle est importante, elle te permet de faire ponctuellement des actions pour lesquelles sur ton ordinateur il faudrait que tu sois administrateur (on dit `root` sous Linux).

Tu n'as pas le droit, de base, d'écrire dans les sous-répertoires de `/etc`.
Du coup, tu es obligé d'utiliser `sudo`.

### Comprendre le fichier de configuration

Puis, tu vas copier le fichier de règles que j'ai créé pour toi, et le mettre à la place. Avant cela, examine-le un peu. La règle importante est celle la:


```
# Ajouter un chiffre apres
Az"[0-9]"
```

Cela veut dire:

1. Dans la liste de mots, copie tous les caractères de A à Z et a à z. Par exemple, si le mot est `pomme`, tous les caractères sont entre a et z, donc cela les copie tous: `pomme`
2. Ajoute un chiffre compris entre 0 et 9.

### Copier le fichier de configuration

Copie le fichier `john.conf` que tu as téléchargé vers `/etc/john/john.conf`:

```
sudo cp john.conf /etc/john/john.conf
```

Pour que cette commande marche, il faut que tu sois dans le répertoire où tu as téléchargé le fichier `john.conf`. Tu peux changer de répertoire avec la commande `cd`.

### Lancer john

Ensuite, on demande à john de tester avec le mot de passe ainsi créé. Pour cela, il faut que tu tapes dans un terminal:

```
john mdphache --wordlist=talistedefruits --rules
```

Attention, c'est à toi d'écrire une jolie liste de fruits, en anglais.
Bonne chance !


