Title: Mission (Im)possible
Category: Unix
Score: 100
Description:

Ethan, nous avons appris que le "Bone Doctor" prévoir à nouveau des actions musclées contre notre pays, et plus particulièrement contre notre section, l'IMF (Impossible Missions Force).

Nous vous avons récupéré le login du Bone Doctor. A vous de trouver ce qu'il prépare...

## Accès à la machine de Bone Doctor

- Adresse IP: A REMPLIR
- port: 2922
- utilisateur: `bonedoc`
- mot de passe: `synd1cate`

## History, un keylogger sans avoir besoin de coder

Je pense qu'il est inutile de vous rappeler, Ethan, que sous Unix, ce que vous tapez dans votre shell se retrouve mémorisé dans un *historique*. Cet historique, on peut y accéder via la commande `history`.

Exemple: 

```
$ history
...
  539  make build
  540  make build
  541  make build
  542  make run
```

C'est meme très pratique, car on peut rappeler une commande effectuée dans l'historique en tapant `!num` où l'on remplace `num` par le nombre dans l'historique.
Par exemple, dans l'exemple au dessus, si on tape `!542`, cela fait `make run`.
Si on tape `!539`, ça fait `make build`. C'est particulièrement pratique pour les longues commandes.
