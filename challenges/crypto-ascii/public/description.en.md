Title: ASCII Table
Category: Crypto
Score: 30
Description:

ASCII ruleZ it all on computers, I tell you!
A = 65 = 0x41 (in hex - that's base 16)
B = 66 = 0x42 
...

See https://www.asciitable.com/

Can you decode this? `66 6c 61 67 7b 74 68 69 73 5f 69 73 5f 61 73 63 69 69 7d`

