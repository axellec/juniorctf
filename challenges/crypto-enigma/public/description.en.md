Title: WWII
Category: Crypto
Score: 40
Description:

Wikipedia says: "The Enigma machine is an encryption device developed and used in the early- to mid-20th century to protect commercial, diplomatic and military communication. It was employed extensively by Nazi Germany during World War II, in all branches of the German military. "

Can you decode this?

`RBPMVUWBPBEPRYQGHJDC`

Separate each words with a `_` to commit the flag.
