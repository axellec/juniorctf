var count = 0;

function guess(){
    var iq = document.getElementById("iq").value;
    var guess_this = Math.floor((Math.random() * 220) + 1);

    if (iq == guess_this) {
	count = count + 1;
	if (count < 3) {
	    document.getElementById("result").innerHTML = "Nice. Can we try again? I want to be sure for your IQ. ";	    
	} else {
	    var flag = decodeURIComponent("flag%7B%40re%23you%23bo%40sting%3F%7D");
	    document.getElementById("result").innerHTML = "Congrats. Your flag: " + flag;
	}
    } else {
	document.getElementById("result").innerHTML = "Looks like you are not that smart!?";
	count = 0;
    }
    
    return false;
}

