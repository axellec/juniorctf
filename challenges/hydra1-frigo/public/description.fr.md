Title: Le frigo de Pico
Category: Passwords
Score: 100
Description:

[Pico le Croco](http://picolecroco.free.fr) tient beaucoup à son champagne.
Il le conserve, au frais, dans son frigo.
Seulement, voilà. Nous, on vient de faire une longue randonnée (20 kms), et on a très très soif. On voudrait bien boire un petit coup. Mais... arg... Pico a protégé son frigo par mot de passe ! Vous y croyez vous ? Ben si !!!

Ta mission: ouvrir le frigo pour boire le champagne de Pico.

## Ouvrir la Porte du frigo

On accède à la porte du frigo de Pico via SSH. Tu te rappelles comment faire ?

```
- Nom du frigo: ADRESSE IP
- Port: 2932
- identifiant: pico
```

A toi de trouver le mot de passe !

## Rappels pour la vraie vie

- Quand tu as soif, c'est mieux de prendre de l'eau ;)
- C'est **interdit** d'accéder à des machines si on ne t'y a pas autorisé. Non vraiment.

## Indice

Un indice pour le mot de passe ? Non ! Dans la "vraie vie", tu n'auras pas forcément des indices !

## La force de la brute

Pour résoudre cette énigme, je te conseille d'utiliser la méthode dite "brute force" (en anglais). Non non, ça ne se traduit pas vraiment par "la force de la brute", mais c'est l'idée.

Tu pourrais tout simplement essayer tout plein de mots de passe pour ouvrir la porte du frigo. Jusqu'à ce que tu tombes sur le bon. A moins d'avoir une chance insolente, cela risque de te prendre pas mal de temps.

Encore une fois, on va etre un peu paresseux, et c'est un programme qui va faire le travail à ta place. C'est lui qui va tester pour toi des centaines de mots de passe. Ce programme s'appelle **Hydra**.

## Hydra

Installe **Hydra** sur ta machine:

```
sudo apt-get install hydra hydra-gtk
```

Regardons un peu ce que fait hydra... 


## Manuel

La quasi totalité des commandes et programmes Unix viennent avec des "pages de manuel". C'est une documentation succinte et pas du tout poétique de comment utiliser la commande / programme.

Essaie 

```
man hydra
```

**man** est la commande Unix pour montrer une page de manuel. On tape *hydra* après pour avoir la documentation d'Hydra. **Ceci doit devenir un réflexe** pour toi lorsque tu ne sais pas comment marche une commande, un programme, une fonction sous Unix.

Toutes les pages de manuel commencent en haut par une brève description de la chose:

```
HYDRA(1)                    General Commands Manual                   HYDRA(1)

NAME
       hydra  - a very fast network logon cracker which support many different
       services
```

Ensuite, tu as quelque chose qui t'explique les options possibles.

```
SYNOPSIS
       hydra
        [[[-l LOGIN|-L FILE] [-p PASS|-P FILE|-x OPT]] | [-C FILE]] [-e nsr]
        [-u] [-f] [-F] [-M FILE] [-o FILE] [-t TASKS] [-w TIME] [-W TIME]
        [-s PORT] [-S] [-4/6] [-vV] [-d]
        server service [OPTIONAL_SERVICE_PARAMETER]
```

- Quand il y a des crochets autour d'une option, c'est qu'elle est *optionnelle*
- Sinon elle doit etre présente
- L'ordre des paramètres doit etre respecté a priori

Par exemple, tu vois ici que hydra devra toujours avoir un *server* et un *service*, tandis que par exemple le nom de login "-l LOGIN" est *optionel*.

Enfin la suite du manuel t'explique chaque paramètre un à un.

## Je veux attaquer !

Nous souhaitons essayer une multitude de mots de passe sur le frigo de pico. Cela veut dire qu'il faut essayer Hydra avec:

- le service, ce sera **ssh**
- le serveur, ce sera **ADRESSE IP**
- le login, c'est l'utilisateur avec lequel se connecter, donc **pico**
- les mots de passe à tester: je te fournis une liste des 500 mots de passe les plus fréquents. Télécharge la et utilise la avec Hydra.

Ta commande ressemblera donc à :

```bash
$ hydra -l pico -P ./500-worst-passwords.txt ADRESSE ssh
```

En fait, ce n'est *pas tout à fait ça*. Il te manque une option à trouver pour spécifier le **numéro de port** qui est 2932. Je te laisse chercher !

## Au cas où

Si jamais avec Hydra tu tombes sur beaucoup de :

```
[ERROR] ssh protocol error
[ERROR] ssh protocol error
[ERROR] ssh protocol error
[ERROR] ssh protocol error
```

pense à essayer de te logger une première fois manuellement sur le frigo de Pico, puis reéessaie Hydra.
