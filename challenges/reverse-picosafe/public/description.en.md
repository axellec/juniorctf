Title: Pico's Safe
Category: Reverse
Score: 200
Description:

** Solve Getin 3 before you try this one **

Try and open Pico's safe to grab all his $$$$ !

His PIN code is 4 digit long.

Good luck.

Download the executable `picosafe`, make it executable and run it.

## Disassembly Tips

- To analyze a program in radare2, use command `aa`
- To list all functions in a program, use command `afl`. We recommend you focus on a function that checks the PIN code ;-) Just sayin'
