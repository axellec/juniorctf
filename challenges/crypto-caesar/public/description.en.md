Title: Ave Caesar
Category: Crypto
Score: 70
Description:

Do you know how people from Caeser's time used to send encrypted messages?

Try to get the flag : `73796e747b706562706271767972665f726e675f7a7266667261747265667d`
