#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from os.path import join, isdir, exists, basename
from os import listdir
from bs4 import BeautifulSoup
from requests.compat import urljoin

import os
import re
import requests
import argparse
import markdown
import codecs


class Challenge():

    def __init__(self, path):
        self.path = path
        self.lang = [] # supported languages for this challenge
        self._parse_description()
        self._get_flag()
        self._get_files()

    def _parse_marker(self, marker='Title:', text=''):
        begin_index = text.lower().find(marker.lower(), 0, 300)
        if begin_index < 0:
            raise IndexError('Marker not found')
        
        end_index = text.find('\n', begin_index, 300)
        if end_index < 0:
            raise ValueError('Cannot find end of marker %s' % (marker))

        return text[begin_index+len(marker):end_index].strip()

    def _parse_description(self):
        self.description = {}
        self.title = {}
        self.category = {}
        for lang in ['fr', 'en']:
            path = join(self.path, 'public', 'description.%s.md' % lang)
            if exists(path):
                # read file
                input_file = codecs.open(path, mode='r', encoding='utf-8')
                text = input_file.read()

                # extract title, category and value
                self.title[lang] = self._parse_marker('Title:', text)
                self.category[lang] = self._parse_marker('Category:', text)
                self.value = self._parse_marker('Score: ', text)

                # if present, extract type
                self.type = 'standard'
                try:
                    self.type = self._parse_marker('Type:', text)
                except IndexError:
                    pass # standard is default

                index = text.find('Description:', 0, 300)
                html = markdown.markdown(text[index+len('Description:'):].strip())
                self.description[lang] = html
                
                if lang not in self.lang:
                    self.lang.append(lang)

    def _get_files(self):
        # exclude description files
        blacklist = r'description\.[a-zA-Z]{2}\.md'
        
        self.files = [join(self.path, 'public', f) for f in listdir(join(self.path, 'public'))
                if not re.match(blacklist, basename(f))]

    def _get_flag(self):
        self.flag = {}
        for lang in ['fr', 'en']:
            path = join(self.path, 'FLAG.%s' % lang)
            if exists(path):
                input_file = codecs.open(path, mode='r', encoding='utf-8')
                self.flag[lang] = input_file.read().strip()

    def __str__(self):
        if self.lang and self.title[self.lang[0]]:
            return '[Challenge : %s]' % self.title[self.lang[0]]
        return '[Challenge: unknown title]'


class Importer():

    def __init__(self):
        self.parser = self._create_parser()
        self.args = self.parser.parse_args()

        # get challenges
        self.challenges = self._get_challenges()

        # list challenges
        if self.args.list:
            self.list_challenges(self.args.list)
            return 

        self.session = requests.Session()
        if not self.args.passw:
            self.parser.error('No password provided, use --pass')

        if self.check_setup():
            if not self.args.setup:
                self.parser.error('CTFd is not initialized. Use the --setup argument to perform automatic setup')
            else:
                self.perform_setup()

        # login and import challenges
        self._login()
        self.import_challenges(self.args.lang)

    def list_challenges(self, lang):
        for challenge in self.challenges:
            if lang in challenge.lang:
                print('[*] %s' % challenge.title[lang])

    def check_setup(self):
        r = requests.get(self.args.url, allow_redirects=False)
        return r.status_code == 302 and r.headers['location'].endswith('/setup')

    def perform_setup(self):
        if not self.args.email:
            self.parser.error('No admin email provided, use --email')
        url = urljoin(self.args.url, 'setup')
        
        # get nonce
        r = self.session.get(url)
        soup = BeautifulSoup(r.text, features="html.parser")
        nonce = soup.find('input', {'name': 'nonce'}).get('value')

        form_data = {
                'ctf_name': 'juniorctf',
                'name': self.args.login,
                'email': self.args.email,
                'password': self.args.passw,
                'user_mode': 'users',
                'nonce': nonce
            }
        r = self.session.post(url, data=form_data)
        r.raise_for_status()
        print('[*] Setup CTFd complete')
        print('    Login with user "%s"' % self.args.login)

    def import_challenges(self, lang):
        for challenge in self.challenges:
            if lang in challenge.lang:
                print("[*] importing %s" % challenge.title[lang])
                challenge_id = self._post_challenge_description(challenge, lang)
                self._post_challenge_flag(challenge_id, challenge, lang)
                self._post_challenge_files(challenge_id, challenge)

    def _get_challenges(self):
        # folders to exclude
        blacklist = ['sysadmin']

        # get all challenges from 'challenges' subfolder
        base = join(self.args.basedir, 'challenges')
        dirnames = [d for d in os.listdir(base)
                        if isdir(join(base, d)) and d not in blacklist]

        # intersect with challenges provided as commandline arguments
        if self.args.challenge:
            dirnames = [d for d in self.args.challenge if d in dirnames]

        return [Challenge(join(base, d)) for d in dirnames]

    def _create_parser(self):
        parser = argparse.ArgumentParser(description='Import Junior CTF challenges in CTFd',
                formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument('-b', '--basedir', help='Junior CTF base directory', default='/opt/juniorctf')
        parser.add_argument('-u', '--url', help='CTFd URL', default='http://juniorctf/')
        parser.add_argument('-l', '--login', help='CTFd admin user', default='admin')
        parser.add_argument('-p', '--pass', dest='passw', metavar='PASS', help='CTFd admin pass')
        parser.add_argument('--setup', action='store_true', help='Setup CTFd using the provided login/password as admin credentials')
        parser.add_argument('--email', help='Admin email address to use for CTFd setup')
        parser.add_argument('--lang', help='Junior CTF language (currently supported: fr, en)', default='fr')
        parser.add_argument('--list', action='store_true', help='List available challenges')
        parser.add_argument('challenge', nargs='*', help='Challenges to import. All challenges are imported if nothing specified')
        return parser

    def _login(self):
        url = urljoin(self.args.url, 'login')
        
        # get nonce
        r = self.session.get(url)
        soup = BeautifulSoup(r.text, features="html.parser")
        nonce = soup.find('input', {'name': 'nonce'}).get('value')
        
        # login as admin
        login_data = {'name': self.args.login, 'password': self.args.passw, 'nonce': nonce}
        r = self.session.post(url, data=login_data)
        
        r.raise_for_status()
        if "Your username or password is incorrect" in r.text:
            raise ValueError("Your username or password is incorrect")

        soup = BeautifulSoup(r.text, features="html.parser")
        self.csrf_nonce = soup.find('input', {'name': 'nonce'}).get('value') # nonce  has been updated with CSRF nonce value
        print("CSRF Nonce: ", self.csrf_nonce)

        print("[*] logged in as %s" % self.args.login)

    def _post_challenge_description(self, challenge, lang):
        url = urljoin(self.args.url, 'api/v1/challenges')
        challenge_data = {
                'name': challenge.title[lang],
                'category': challenge.category[lang],
                'description': challenge.description[lang],
                'value': challenge.value,
                'state': 'visible',
                'type': 'standard'
            }

        r = self.session.post(url, headers={"CSRF-Token" : self.csrf_nonce }, json=challenge_data)
        r.raise_for_status()

        return r.json()['data']['id']

    def _post_challenge_flag(self, challenge_id, challenge, lang):
        url = urljoin(self.args.url, 'api/v1/flags')
        flag_data = {
                'content': challenge.flag[lang],
                'type': 'static',
                'challenge': challenge_id
            }
        r = self.session.post(url, headers={"CSRF-Token" : self.csrf_nonce }, json=flag_data)
        r.raise_for_status()

    def _post_challenge_files(self, challenge_id, challenge):
        for f in challenge.files:
            # get nonce
            url = urljoin(self.args.url, 'admin/challenges', challenge_id)
            r = self.session.get(url)
            r.raise_for_status()
            nonce = re.search(r'var csrf_nonce = "([^"]*)";', r.text).group(1)

            # see: https://stackoverflow.com/questions/22567306/python-requests-file-upload
            url = urljoin(self.args.url, 'api/v1/files')
            files = {'file': open(f, 'rb')}
            data = {
                    'nonce': nonce,
                    'challenge': challenge_id,
                    'type': 'challenge'
                }
            r = self.session.post(url, headers={"CSRF-Token" : self.csrf_nonce },files=files, data=data)
            r.raise_for_status()

def main():
    importer = Importer()

if __name__ == '__main__':
    main()

