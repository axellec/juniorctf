#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# apt-get install python3-bs4

from requests.compat import urljoin
from bs4 import BeautifulSoup

import argparse
import requests
import re
import json
import urllib3

class Importer():

    def __init__(self):
        self.args = self.get_arguments()
        self.session = requests.Session()
        self.challenges = []
        
    def get_arguments(self):
        parser = argparse.ArgumentParser(description='Delete challenges from CTFd server')
        parser.add_argument('-u', '--url', help='CTFd server URL')
        parser.add_argument('-l', '--login', help='CTFd admin user', default='admin')
        parser.add_argument('-p', '--password', help='CTFd admin password')
        return parser.parse_args()

    def login(self):
        url = urljoin(self.args.url, 'login')

        # get nonce
        r = self.session.get(url, verify=False)
        soup = BeautifulSoup(r.text, features="html.parser")
        nonce = soup.find('input', {'name': 'nonce'}).get('value')

        # login as admin
        login_data = {'name': self.args.login, 'password': self.args.password, 'nonce': nonce}
        print("Login data JSON: ",login_data)
        r = self.session.post(url, data=login_data, verify=False)
        
        r.raise_for_status()
        if "Your username or password is incorrect" in r.text:
            print("[-] ERROR: ",r.text)
            raise ValueError("Your username or password is incorrect")

        csrf_nonce = re.search(r"csrf_nonce\s*=\s*\"(.*)\";", r.text).group(1)
        print("CSRF Nonce: ", csrf_nonce)

        print("[*] logged in as %s" % self.args.login)

    def list_challenges(self):
        # get nonce
        url = urljoin(self.args.url, 'admin/challenges')
        r = self.session.get(url)
        r.raise_for_status()
        csrf_nonce = re.search(r'var csrf_nonce = "([^"]*)";', r.text).group(1) 
        print("CSRF Token: ", csrf_nonce)

        # get challenge ids
        matches = re.findall(r'a href="/admin/challenges/([0-9]*)', r.text)

        # get unique and integer ids
        challenge_ids = [ int(x) for x in list(set(matches)) ]
        return challenge_ids

    def delete_challenge(self, challenge_id):
        # get nonce
        url = urljoin(self.args.url, 'admin/challenges/{0}'.format(challenge_id))
        r = self.session.get(url, verify=False)
        r.raise_for_status()
        csrf_nonce = re.search(r'var csrf_nonce = "([^"]*)";', r.text).group(1) 
        print("CSRF Token: ", csrf_nonce)

        # delete
        url = urljoin(self.args.url, 'api/v1/challenges/{0}'.format(challenge_id))
        print(url)
        r = self.session.delete(url, headers={"CSRF-Token" : csrf_nonce }, json={}, verify=False)
        r.raise_for_status()
        print("[*] Deleted challenge id ", challenge_id)

if __name__ == '__main__':
        urllib3.disable_warnings() # Disables SSL warnings - beware
        importer = Importer()
        importer.login()
        challenge_list = importer.list_challenges()
        for challenge_id in challenge_list:
            answer = input("Do you wish to delete challenge id {0}? (y/N) ".format(challenge_id))
            if answer.lower() == 'y' or answer.lower() == 'yes':
                importer.delete_challenge(challenge_id)
            else:
                print("Skipping challenge ", challenge_id)
